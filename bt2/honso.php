<?php 
class HonSo{
    private $__phanNguyen;
    private $__phanTu;
    private $__phanMau;
    
    public function setPhanNguyen($phanNguyen){
        $this->__phanNguyen = $phanNguyen;
    }
    public function getPhanNguyen(){
        return $this->__phanNguyen;
    }
    public function setPhanTu($phanTu){
        $this->__phanTu = $phanTu;
    }
    public function getPhanTu(){
        return $this->__phanTu;
    }
    public function setPhanMau($phanMau){
        $this->__phanMau = $phanMau;
    }
    public function getPhanMau(){
        return $this->__phanMau;
    }

    public function nhapHonSo($nguyen,$tu,$mau){
        $this->setPhanNguyen($nguyen);
        $this->setPhanTu($tu);
        $this->setPhanMau($mau);
    }
    public function UCLN($a,$b){
        $m = (int)abs($a);
        $n = (int)abs($b);
        while($m * $n != 0){
            if($m > $n){
                $m = $m - $n;
            }else{
                $n = $n -$m;
            }
        }
        return $m + $n;
    }
    public function doiHonSo($nguyen,$tu,$mau){
        $this->setPhanTu($nguyen*$mau+$tu);
        $this->setPhanMau($mau);
    }
    public function doiPhanSo($tu,$mau){
        $this->setPhanNguyen(($tu<0)?ceil($tu/$mau):floor($tu/$mau));
        $this->setPhanTu(abs($tu % $mau));
        $this->setPhanMau($mau);
    }
    public function rutGon(){
        $ucln = $this->UCLN($this->getPhanTu(),$this->getPhanMau());
        $this->setPhanTu($this->getPhanTu()/$ucln);
        $this->setPhanMau($this->getPhanMau()/$ucln);
    }
   
}

