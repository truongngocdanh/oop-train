<?php
include "honso.php";
class Tinh extends HonSo
{
    public function xuatHonSo()
    {
        if (isset($_POST['btn'])) {
            $honSo1 = new HonSo();
            $honSo2 = new HonSo();
            $honSo1->nhapHonSo($_POST['nguyenhs1'],$_POST['tuhs1'],$_POST['mauhs1']);
            $honSo2->nhapHonSo($_POST['nguyenhs2'],$_POST['tuhs2'],$_POST['mauhs2']);
            return array(
                'nguyenhs1' => $honSo1->getPhanNguyen(),
                'tuhs1' => $honSo1->getPhanTu(),
                'mauhs1' => $honSo1->getPhanMau(),
                'nguyenhs2' => $honSo2->getPhanNguyen(),
                'tuhs2' => $honSo2->getPhanTu(),
                'mauhs2' => $honSo2->getPhanMau()
            );
        }
    }

    public function congHaiHonSo(){
        $honSo1 = new HonSo();
        $honSo2 = new HonSo();
        $honSo1->nhapHonSo($_POST['nguyenhs1'],$_POST['tuhs1'],$_POST['mauhs1']);
        $honSo2->nhapHonSo($_POST['nguyenhs2'],$_POST['tuhs2'],$_POST['mauhs2']);
        $honSo1->doiHonSo($honSo1->getPhanNguyen(),$honSo1->getPhanTu(),$honSo1->getPhanMau());
        $honSo2->doiHonSo($honSo2->getPhanNguyen(),$honSo2->getPhanTu(),$honSo2->getPhanMau());
        $this->setPhanTu($honSo1->getPhanTu()*$honSo2->getPhanMau()+$honSo2->getPhanTu()*$honSo1->getPhanMau());
        $this->setPhanMau($honSo1->getPhanMau()*$honSo2->getPhanMau());
        $this->doiPhanSo($this->getPhanTu(),$this->getPhanMau());
        return $data=array(
            'nguyen'=>$this->getPhanNguyen(),
            'tu'=>$this->getPhanTu(),
            'mau'=>$this->getPhanMau()
        );
    }
    public function truHaiHonSo(){
        $honSo1 = new HonSo();
        $honSo2 = new HonSo();
        $honSo1->nhapHonSo($_POST['nguyenhs1'],$_POST['tuhs1'],$_POST['mauhs1']);
        $honSo2->nhapHonSo($_POST['nguyenhs2'],$_POST['tuhs2'],$_POST['mauhs2']);
        $honSo1->doiHonSo($honSo1->getPhanNguyen(),$honSo1->getPhanTu(),$honSo1->getPhanMau());
        $honSo2->doiHonSo($honSo2->getPhanNguyen(),$honSo2->getPhanTu(),$honSo2->getPhanMau());
        $this->setPhanTu($honSo1->getPhanTu()*$honSo2->getPhanMau()-$honSo2->getPhanTu()*$honSo1->getPhanMau());
        $this->setPhanMau($honSo1->getPhanMau()*$honSo2->getPhanMau());
        $this->doiPhanSo($this->getPhanTu(),$this->getPhanMau());
        return $data=array(
            'nguyen'=>$this->getPhanNguyen(),
            'tu'=>$this->getPhanTu(),
            'mau'=>$this->getPhanMau()
        );
    }
    public function nhanHaiHonSo(){
        $honSo1 = new HonSo();
        $honSo2 = new HonSo();
        $honSo1->nhapHonSo($_POST['nguyenhs1'],$_POST['tuhs1'],$_POST['mauhs1']);
        $honSo2->nhapHonSo($_POST['nguyenhs2'],$_POST['tuhs2'],$_POST['mauhs2']);
        $honSo1->doiHonSo($honSo1->getPhanNguyen(),$honSo1->getPhanTu(),$honSo1->getPhanMau());
        $honSo2->doiHonSo($honSo2->getPhanNguyen(),$honSo2->getPhanTu(),$honSo2->getPhanMau());
        $this->setPhanTu($honSo1->getPhanTu()*$honSo2->getPhanTu());
        $this->setPhanMau($honSo1->getPhanMau()*$honSo2->getPhanMau());
        $this->doiPhanSo($this->getPhanTu(),$this->getPhanMau());
        return $data=array(
            'nguyen'=>$this->getPhanNguyen(),
            'tu'=>$this->getPhanTu(),
            'mau'=>$this->getPhanMau()
        );
    }
    public function chiaHaiHonSo(){
        $honSo1 = new HonSo();
        $honSo2 = new HonSo();
        $honSo1->nhapHonSo($_POST['nguyenhs1'],$_POST['tuhs1'],$_POST['mauhs1']);
        $honSo2->nhapHonSo($_POST['nguyenhs2'],$_POST['tuhs2'],$_POST['mauhs2']);
        $honSo1->doiHonSo($honSo1->getPhanNguyen(),$honSo1->getPhanTu(),$honSo1->getPhanMau());
        $honSo2->doiHonSo($honSo2->getPhanNguyen(),$honSo2->getPhanTu(),$honSo2->getPhanMau());
        $this->setPhanTu($honSo1->getPhanTu()*$honSo2->getPhanMau());
        $this->setPhanMau($honSo1->getPhanMau()*$honSo2->getPhanTu());
        $this->doiPhanSo($this->getPhanTu(),$this->getPhanMau());
        return $data=array(
            'nguyen'=>$this->getPhanNguyen(),
            'tu'=>$this->getPhanTu(),
            'mau'=>$this->getPhanMau()
        );
    }
}


$obj = new Process();
$data['honso'] = $obj->xuatHonSo();
$data['cong']=$obj->congHaiHonSo();
$data['tru']=$obj->truHaiHonSo();
$data['nhan']=$obj->nhanHaiHonSo();
$data['chia']=$obj->chiaHaiHonSo();
?>
<div class="result">
    <h3>Hon so 1</h3>
    <div class="row">
        phan nguyen 1: <?=$data['honso']['nguyenhs1']?><br>
        phan tu 1: <?=$data['honso']['tuhs1']?><br>
        phan mau 1:<?=$data['honso']['mauhs1']?>
    </div>
    <hr>
    <h3>Hon so 2</h3>
    <div class="row">
        phan nguyen 2: <?=$data['honso']['nguyenhs2']?><br>
        phan tu 2: <?=$data['honso']['tuhs2']?><br>
        phan mau 2:<?=$data['honso']['mauhs2']?>
    </div>
    <hr>
    <h3>Ket qua</h3>
    <div class="row">
        ket qua cong: <?=$data['cong']['nguyen']?> + <?=$data['cong']['tu']?> / <?=$data['cong']['mau']?>
    </div>
    <div class="row">
        ket qua tru:  <?=$data['tru']['nguyen']?> + <?=$data['tru']['tu']?> / <?=$data['tru']['mau']?>
    </div>
    <div class="row">
        ket qua nhan: <?=$data['nhan']['nguyen']?> + <?=$data['nhan']['tu']?> / <?=$data['nhan']['mau']?>
    </div>
    <div class="row">
        ket qua chia:<?=$data['chia']['nguyen']?> + <?=$data['chia']['tu']?> / <?=$data['chia']['mau']?>
    </div>
</div>