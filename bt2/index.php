<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bài tập 1</title>
</head>
<body>
    <div class="wrap">
        <h1>Chương trình tính hỗn số</h1>
        <form action="tinh.php" method="post">
            <div class="hs1">
                <h2>
                    hỗn số 1
                </h2>
                <div class="row">
                    phan nguyen: <input type="text" name="nguyenhs1">
                </div>
                <div class="row">
                    phan tu: <input type="text" name="tuhs1">
                </div>
                <div class="row">
                    phan mau: <input type="text" name="mauhs1">
                </div>
            </div>
            <hr>
            <div class="hs2">
                <h2>hon so 2</h2>
                <div class="row">
                    phan nguyen: <input type="text" name="nguyenhs2">
                </div>
                <div class="row">
                    phan tu: <input type="text" name="tuhs2">
                </div>
                <div class="row">
                    phan mau: <input type="text" name="mauhs2">
                </div>
            </div>
            <div class="btn">
                <input type="submit" value="Result" name="btn" id="btn">
            </div>
        </form>
    </div>
</body>
</html>