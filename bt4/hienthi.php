<?php 
include "thongtin.php";
$obj = new ThongTin();
if(isset($_POST['btn'])){
    $obj -> nhapThongTin($_POST['ten'],$_POST['cmnd'],$_POST['quequan']);
    $data = $obj->xuatThongTin();
}else{
    die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hiển thị thông tin</title>
</head>
<body>
    <div class="wrap">
        <h1>Hiển thị thông tin</h1>
        <div class="row">
            Họ và Tên: <?=$data['ten']?>
        </div>
        <hr> 
        <div class="row">
            Số chứng minh: <?=$data['cmnd']?>
        </div>
        <hr>
        <div class="row">
            Quê quán: <?=$data['quequan']?>
        </div>
        <hr>
    </div>
</body>
</html>