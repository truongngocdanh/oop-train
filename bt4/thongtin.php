<?php 
class ThongTin{
    private $__hoTen;
    private $__cmnd;
    private $__queQuan;

    public function setTen($ten){
        $this->__hoTen = $ten;
    }
    public function getTen(){
        return $this->__hoTen;
    }
    public function setCmnd($cmnd){
        $this->__cmnd = $cmnd;
    }
    public function getCmnd(){
        return $this->__cmnd;
    }
    public function setQueQuan($queQuan){
        $this->__queQuan = $queQuan;
    }
    public function getQueQuan(){
        return $this->__queQuan;
    }
    public function nhapThongTin($ten,$cmnd,$que){
        $this->setTen($ten);
        $this->setCmnd($cmnd);
        $this->setQueQuan($que);
    }
    public function xuatThongTin(){
        return array(
            'ten' => $this->getTen(),
            'cmnd' => $this->getCmnd(),
            'quequan' => $this->getQueQuan()
        );
    }
}