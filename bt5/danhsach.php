<?php 
    include "sinhvien.php";
    $obj = new SinhVien;
    $data = $obj->danhSachSV();
    $data2 = $obj->laySVCungQue();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Danh sach</title>
</head>
<body>
    <div class="wrap" style="width:600px;margin:auto;">
        <table border="1" width="100%">
            <thead>
                <tr>
                    <td colspan="4" style="text-align:center">Danh sach sinh vien</td>
                </tr>
                <tr>
                    <td>mssv</td>
                    <td>Ho Ten</td>
                    <td>Que quan</td>
                    <td>Nam sinh</td>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($data as $item){
                ?>     
                <tr>
                    <td><?=$item['MSSV']?></td>
                    <td><?=$item['HoTen']?></td>
                    <td><?=$item['QueQuan']?></td>
                    <td><?=$item['NamSinh']?></td>
                </tr>          
                <?php }?>
            </tbody>
        </table>
        <?php 
            foreach($data2 as $item){
            $i = 0;
            echo "<p> Các sinh viên ";
                foreach($item as $item2){
                    echo $item2['HoTen'].", ";
                }
            echo " có cùng quê là ".$item[$i]['QueQuan']."</p>";
            }
            $i++;
        ?>
    </div>
</body>
</html>
