<?php 
include "database.php";
class SinhVien extends Database{
    private $__MSSV;
    private $__hoTen;
    private $__ngaySinh;
    private $__queQuan;
    private $__result;
    public function __construct()
    {
        $this->getConnect();
    }
    public function setMSSV($mssv){
        $this->__MSSV = $mssv;
    }
    public function getMSSV(){
        return $this->__MSSV;
    }
    public function setHoTen($HoTen){
        $this->__hoTen = $HoTen;
    }
    public function getHoTen(){
        return $this->__hoTen;
    }
    public function setNgaySinh($ngaySinh){
        $this->__ngaySinh = $ngaySinh;
    }
    public function getNgaySinh(){
        return $this->__ngaySinh;
    }
    public function setQueQuan($QueQuan){
        $this->__queQuan = $QueQuan;
    }
    public function getQueQuan(){
        return $this->__queQuan;
    }
    public function checkMSSV($mssv){
        $sql = "select * from sinhvien where MSSV='".$mssv."'";
        $row = $this->query($sql);
        if(mysqli_num_rows($row) > 0){
            return FALSE;
        }else{
            return TRUE;
        }
    }
    public function insertSinhVien(){
        $sql = "INSERT INTO sinhvien (MSSV,HoTen,QueQuan,NamSinh) VALUES ('".$this->getMSSV()."','".$this->getHoTen()."','".$this->getQueQuan()."','".$this->getNgaySinh()."')";
        $this->query($sql);
    }
    public function danhSachSV(){
        $sql = "select * from sinhvien order by NamSinh asc";
        $this->setResult($this->query($sql));
        return $this->allRow();
    }
    public function laySVCungQue(){
        $sql = "SELECT QueQuan FROM sinhvien GROUP BY QueQuan HAVING COUNT(1) > 1";
        $this->setResult($this->query($sql));
        $data = $this->allRow();
        $data2 = array();
        foreach($data as $item){
            $sql = "select HoTen,QueQuan from sinhvien where QueQuan = '".$item['QueQuan']."'";
            $this->setResult($this->query($sql));
            $row = $this->allRow();
            $data2[] = $row;
        }
        return $data2;
    }
}