<?php 
    include "sinhvien.php";
    if(isset($_POST['add'])){
        $mssv=$hoten=$ngaysinh=$quequan="";
        if($_POST['mssv']!=""){
            $mssv = $_POST['mssv'];
        }
        if($_POST['hoten']!=""){
            $hoten = $_POST['hoten'];
        }
        if($_POST['ngaysinh']!=""){
            $ngaysinh = $_POST['ngaysinh'];
        }
        if($_POST['quequan']!=""){
            $quequan = $_POST['quequan'];
        }
        if($mssv && $hoten && $ngaysinh && $quequan){
            $obj = new SinhVien();
            if($obj->checkMSSV($mssv) == FALSE){
                echo "Mã số sinh viên đã có";
            }else{
                $obj->setMSSV($mssv);
            }
            $obj->setHoTen($hoten);
            $obj->setNgaySinh($ngaysinh);
            $obj->setQueQuan($quequan);
            $obj->insertSinhVien();
            header('Location:danhsach.php');
        }else{
            echo 'da co loi xay ra @@';
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Them</title>
</head>
<body>
    <h1>Them sinh vien</h1>
    <div class="wrap">
        <form action="themsinhvien.php" method="post">
            <div class="row">
                Ma sinh vien: <input type="text" name="mssv">
            </div>
            <div class="row">
                Ho ten sinh vien: <input type="text" name="hoten">
            </div>
            <div class="row">
                Nam sinh: <input type="text" name="ngaysinh">
            </div>
            <div class="row">
                Que quan: <input type="text" name="quequan">
            </div>
            <div class="row">
                <input type="submit" name="add" value="ADD">
            </div>
        </form>
    </div>
</body>
</html>