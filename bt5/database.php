<?php 
class Database{
    private $__conn;
    private $__svName = 'localhost';
    private $__svUserName = 'root';
    private $__svPassword = '';
    private $__dbName = 'bt5';
    private $__result;

    public function setResult($result){
        $this->__result = $result;
    }
    public function getResult(){
        return $this->__result;
    }
    public function getConnect(){
        return $this->__conn = mysqli_connect($this->__svName, $this->__svUserName, $this->__svPassword, $this->__dbName);
    }
    public function query($sql){
        return mysqli_query($this->getConnect(),$sql);
    }

    public function allRow(){
        if(mysqli_num_rows($this->getResult()) > 0){
            while($row = mysqli_fetch_assoc($this->getResult())){
                $data[] = $row;
            }
            return $data;
        }else{
            return FALSE;
        }
    }
}
