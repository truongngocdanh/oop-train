<?php 
include 'tinh.php';
$obj = new Tinh($_POST['cao'],$_POST['bankinh']);
$data['thongtin']= $obj->xuatHinhTru();
$data['dientich']=$obj->tinhDienTichToanPhan();
$data['thetich']=$obj->tinhTheTich();
?>
<!DOCTYPE html>
<html lang="en">
    <style>
        .wrap{
            width:500px;
            margin:50px auto 0;
        }
    </style>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kết quả</title>
</head>
<body>
    <div class="wrap">
        <h1>Thông tin hình trụ</h1>
        <div class="row">
            Bán kính: <?=$data['thongtin']['cao']?>
        </div>
        <hr>
        <div class="row">
            Chiều cao: <?=$data['thongtin']['bankinh']?>
        </div>
        <hr>
        <div class="row">
            Diện tích: <?=$data['dientich']?>
        </div>
        <hr>
        <div class="row">
            Thể tích: <?=$data['thetich']?>
        </div>
    </div>
</body>
</html>