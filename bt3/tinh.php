<?php 
include "hinhtru.php";
class Tinh extends HinhTru{
    public function __construct($cao,$banKinh)
    {
        $this->setChieuCao($cao);
        $this->setBanKinh($banKinh);
    }
    public function xuatHinhTru(){
        return array(
            'cao' => $this->getChieuCao(),
            'bankinh' => $this->getBanKinh()
        );
    }
    public function tinhDienTichXungQuanh(){
        return 2*pi()*$this->getChieuCao()*$this->getBanKinh();
    }
    public function tinhDienTichHaiDay(){
        return 2*pi()*pow($this->getBanKinh(),2);
    }
    public function tinhDienTichToanPhan(){
        return round($this->tinhDienTichXungQuanh() + $this->tinhDienTichHaiDay(),2);
    }
    public function tinhTheTich(){
        return round(pi()*pow($this->getBanKinh(),2)*$this->getChieuCao(),2);
    }
}