<?php
class HinhTru{
    private $__chieuCao;
    private $__banKinh;
    public function setChieuCao($cao){
        $this->__chieuCao = $cao;
    }
    public function getChieuCao(){
        return $this->__chieuCao;
    }
    public function setBanKinh($banKinh){
        $this->__banKinh = $banKinh;
    }
    public function getBanKinh(){
        return $this->__banKinh;
    }
    
}